<?php
class About extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
    }
    function index()
    {
        $this->data["title"] = "Giới thiệu";
        $this->data["temp"] = "site/about/index";
        $this->load->view("site/layout", $this->data);
    }
}
