<?php
class Home extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("Slide_model");
        $this->load->model("Images_model");
        $this->load->model("News_model");
    }

    function index()
    {
        $input['limit'] = array(3, 0);

        $this->data["slide"] = $this->Slide_model->get_list($input);
        $this->data["news"] = $this->News_model->get_list($input);
        
        $input2['where'] = ['status' => 1];
        $this->data["images"] = $this->Images_model->get_list($input2);

        $this->data["title"] = "Trang chủ";
        $this->data["temp"] = "site/home/index";
        $this->load->view("site/layout", $this->data);
    }
}
