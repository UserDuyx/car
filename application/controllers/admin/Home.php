<?php
class Home extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        // $this->load->model("Slide_model");
        // $this->load->model("Product_model");
        // $this->load->model("Post_model");
    }

    function index() 
    {
        $this->data['title'] = "Admin Panel";
        $this->data['temp'] = "admin/home/index";
        $this->load->view("admin/layout", $this->data);
    }
}