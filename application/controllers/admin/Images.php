<?php
class Images extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("Images_model");
        $this->load->library('Upload_library');
    }
    function index()
    {
        $this->data['images'] = $this->Images_model->get_list();

        $this->data["title"] = "Quản Lý Hình Ảnh";
        $this->data["temp"] = "admin/images/index";
        $this->load->view("admin/layout", $this->data);
    }

    function detail()
    {
        $id = $this->uri->rsegment(3);
        if (!empty($id)) {
            $detail = $this->Images_model->get_info(intval($id));
            if (empty($detail))
                redirect(admin_url("images"));
            $this->data['images'] = $detail;
        }
        if ($this->input->post()) {
            $data = array(
                "status" => $this->input->post("status")
            );

            $upload_path = './uploads/images';
            $upload_data = $this->upload_library->upload('image', $upload_path);
            if (isset($upload_data['file_name'])) {
                $data['image'] = $upload_data['file_name'];
            }

            if (!empty($id)) {
                $data['update_at'] = date('Y-m-d H:i:s');
                if ($this->Images_model->update($id, $data)) {
                    $this->session->set_flashdata('message', 'Cập nhật dữ liệu thành công');
                } else {
                    $this->session->set_flashdata('message', 'Không cập nhật được');
                }
                redirect(admin_url('images/index'));
            } else {

                $data['create_at'] = date('Y-m-d H:i:s');
                if ($this->Images_model->create($data)) {
                    $this->session->set_flashdata('message', 'Thêm mới dữ liệu thành công');
                } else {
                    $this->session->set_flashdata('message', 'Không thêm được');
                }
                redirect(admin_url('images/index'));
            }
        }

        $this->data["title"] = "Quản Lý Hình Ảnh";
        $this->data["temp"] = "admin/images/detail";
        $this->load->view("admin/layout", $this->data);
    }

    function delete()
    {
        $id = $this->uri->rsegment(3);
        $this->__delete($id);
        redirect(admin_url('images'));
    }

    private function __delete($id)
    {
        $slide = $this->Images_model->get_info(intval($id));
        if (empty($slide)) {
            redirect(admin_url('images'));
        }

        $this->Images_model->delete($id);
        $slide_image = './uploads/images/' . $slide->image;

        if (file_exists($slide_image)) {
            unlink($slide_image);
        }
    }
}
