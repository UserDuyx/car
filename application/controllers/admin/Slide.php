<?php
class Slide extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Slide_model');
        $this->load->library('Upload_library');
    }

    function index()
    {
        $this->data['slide'] = $this->Slide_model->get_list();
        $this->data['title'] = 'Slide Management';
        $this->data['temp'] = 'admin/slide/index';
        $this->load->view('admin/layout', $this->data);
    }

    function detail()
    {
        $id = $this->uri->rsegment(3);
        if (!empty($id)) {
            $detail = $this->Slide_model->get_info(intval($id));
            if (empty($detail))
                redirect(admin_url("slide"));
            $this->data['slide'] = $detail;
        }

        $data = array();

        $upload_path = './uploads/images';
        $upload_data = $this->upload_library->upload('image', $upload_path);

        if (isset($upload_data['file_name'])) {
            $data['image'] = $upload_data['file_name'];
        }

        if (!empty($id)) {
            $data['update_at'] = date('Y-m-d H:i:s');
            if ($this->Slide_model->update($id, $data)) {
                $this->session->set_flashdata('message', 'Cập nhật dữ liệu thành công');
            } else {
                $this->session->set_flashdata('message', 'Không cập nhật được');
            }
            redirect(admin_url('slide/index'));
        } else {
            $data['create_at'] = date('Y-m-d H:i:s');
            if ($this->Slide_model->create($data)) {
                $this->session->set_flashdata('message', 'Thêm mới dữ liệu thành công');
            } else {
                $this->session->set_flashdata('message', 'Không thêm được');
            }
            redirect(admin_url('slide/index'));
        }


        $this->data['title'] = 'Quản lý Slide';
        $this->data['temp']  = 'admin/slide/detail';
        $this->load->view('admin/layout', $this->data);
    }


    function delete()
    {
        $id = $this->uri->rsegment(3);
        $this->__delete($id);
        redirect(admin_url('slide'));
    }

    private function __delete($id)
    {
        $slide = $this->Slide_model->get_info(intval($id));
        if (empty($slide)) {
            redirect(admin_url('slide'));
        }

        $this->Slide_model->delete($id);
        $slide_image = './uploads/images/' . $slide->image;

        if (file_exists($slide_image)) {
            unlink($slide_image);
        }
    }
}
