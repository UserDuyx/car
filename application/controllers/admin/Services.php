<?php
class Services extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Services_model');
    }

    function index()
    {

        $input = array();
        $list = $this->Services_model->get_list($input);
        $this->data['list'] = $list;

        $this->data['title'] = "Dịch vụ";
        $this->data['temp'] = "admin/services/index";
        $this->load->view("admin/layout", $this->data);
    }

    function detail()
    {

        $id = $this->uri->rsegment(3);
        if (!empty($id)) {
            $detail = $this->Services_model->get_info(intval($id));
            if (empty($detail))
                redirect(admin_url("services"));
            $this->data['detail'] = $detail;
        }

        if ($this->input->post()) {
            $this->form_validation->set_rules('name', 'Tên dịch vụ', 'required', array('required' => '%s không được bỏ trống'));
            $this->form_validation->set_rules('content', 'Nội dung', 'required', array('required' => '%s không được bỏ trống'));

            if ($this->form_validation->run()) {
                //luu du lieu can them
                $data = array(
                    'name'        => $this->input->post('name'),
                    'slug'        => $this->input->post('slug'),
                    'description' => $this->input->post('description'),
                    'content'     => $this->input->post('content'),
                    'status'      => (int)$this->input->post('status'),
                );

                $this->load->library('upload_library');
                $upload_path = './uploads/images';
                $upload_data = $this->upload_library->upload('image', $upload_path);

                if (isset($upload_data['file_name'])) {
                    $data['image'] = $upload_data['file_name'];
                }

                if (!empty($id)) {
                    $data['update_at'] = date('Y-m-d H:i:s');
                    if ($this->Services_model->update($id, $data)) {
                        $this->session->set_flashdata('message', 'Cập nhật dữ liệu thành công');
                    } else {
                        $this->session->set_flashdata('message', 'Không cập nhật được');
                    }
                } else {
                    $data['create_at'] = date('Y-m-d H:i:s');
                    if ($this->Services_model->create($data)) {
                        $this->session->set_flashdata('message', 'Thêm mới dữ liệu thành công');
                    } else {
                        $this->session->set_flashdata('message', 'Không thêm được');
                    }
                }

                //chuyen tới trang danh sách
                redirect(admin_url('services'));
            }
        }

        $this->data['title'] = "Dịch vụ";
        $this->data['temp'] = "admin/services/detail";
        $this->load->view("admin/layout", $this->data);
    }

    function delete()
    {
        $id = $this->uri->rsegment(3);
        $this->__delete($id);
        redirect(admin_url('services'));
    }

    private function __delete($id)
    {
        $services = $this->Services_model->get_info(intval($id));
        if (empty($services)) {
            redirect(admin_url('services'));
        }

        $this->Services_model->delete($id);
        $image = './uploads/images/' . $services->image;

        if (file_exists($image)) {
            unlink($image);
        }
    }
}
