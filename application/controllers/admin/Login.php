<?php
class Login extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("Admin_model");
    }
    function index()
    {
        if ($this->input->post()) {
            $username = array("username" => $this->input->post("username"));
            $password = $this->input->post("password");

            $data = $this->Admin_model->get_info($username);
            if (empty($data)) {
                $this->data["error_login"] = "Username does not exist";
                $this->load->view("admin/login/index", $this->data);
                return;
            }
            if ($data->password != $password && $password != "") {
                $this->data["error_login"] = "Wrong password";
                $this->load->view("admin/login/index", $this->data);
                return;
            }

            $user = array(
                "name" => $data->name,
                "logged_in" => TRUE
            );
            $this->session->set_userdata($user);
            redirect(admin_url());
        }

        $this->load->view("admin/login/index");
    }

    function sign_out()
    {
        $array_items = array('name', 'logged_in');
        $this->session->unset_userdata($array_items);

        redirect(admin_url("login"));
    }
}
