<?php
class MY_Model extends CI_Model
{
    var $table = "";

    /**
     * Get array data from database
     * 
     * @param array $input 
     * @return object $query
     */
    function get_list($input = [])
    {
        //$input["where"] = array ("row condition" => "value")
        if ((isset($input['where'])) && $input['where']) {
            $this->db->where($input['where']);
        }
        //$input["like"] = array ("input1" => "input1")
        if ((isset($input['like'])) && $input['like']) {
            $this->db->like($input['like'][0], $input['like'][1]);
        }
        //$input["order"] = array ("order1", "order2")
        if (isset($input['order'][0]) && isset($input['order'][1])) {
            $this->db->order_by($input['order'][0], $input['order'][1]);
        }
        //$input["limit"] = array ("amount", "start_from")
        if (isset($input['limit'][0]) && isset($input['limit'][1])) {
            $this->db->limit($input['limit'][0], $input['limit'][1]);
        }

        $query = $this->db->get($this->table);
        return $query->result();
    }

    /** 
     * Get information of a certain data by id
     * 
     * @param mixed $input          the value that you want to find 
     * @return object $result
     */

    function get_info($input)
    {
        $where = "";
        if (is_int($input)) {
            $where = array("id" => $input);
        } else {
            $where = $input;
        }

        $result = $this->db->get_where($this->table, $where);
        return $result->row();
    }

    /**
     * Insert an entry into the database
     * 
     * @param array $data data want to add
     */
    function create($data = [])
    {
        if ($this->db->insert($this->table, $data))
            return true;
        return false;
    }

    /**
     * Update an item with speccific data
     * 
     * @param int $id the id of item
     * @param array $data updated data
     */
    function update($id, $data = [])
    {
        $where['id'] = $id;
        if ($this->db->update($this->table, $data, $where))
            return true;
        return false;
    }

    /**
     * Update an item with speccific data
     * 
     * @param int $id the id of item
     * @param array $data updated data
     */
    function delete($id)
    {
        $where['id'] = $id;
        $this->db->delete($this->table, $where);
    }

    function get_total()
    {
        $query = $this->db->get($this->table);

        return $query->num_rows();
    }

    // function get_row_number()
    // {
    //     $sql = "SET @row_number:=0;
    //             SELECT @row_number:=@row_number+1 AS row_number,name, slug FROM " . $this->table . ";";
    //     $query = $this->db->query($sql);
    //     return $query->result();
    // }
}
