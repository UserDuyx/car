<?php
class MY_Controller extends CI_Controller
{
    public $table = array();

    function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');

        $controller = $this->uri->segment(1);
        switch ($controller) {
            case "admin":
                if ($this->uri->segment(3) != "sign_out") {
                    $this->check_login();
                }
            default:
                $this->load->model("Images_model");
                $input['where'] = ['status' => 1];
                $this->data["images"] = $this->Images_model->get_list($input);
        }
    }

    function check_login()
    {
        $user = $this->session->userdata("logged_in");
        $controller = $this->uri->segment(2);

        if ($user == FALSE && $controller != "login") {
            redirect(admin_url("login"));
        } else if ($user == TRUE && $controller == "login") {
            redirect(admin_url("home"));
        }
    }
}
