<?php
class Upload_library
{
    var $CI = '';
    function __construct()
    {
        $this->CI = &get_instance();
    }
    /**
     * Tải lên hình ảnh
     * 
     * @param string $upload_path đường dẫn đến thư mục lưu trữ
     * @param string $file_name tên ảnh
     * @return array $data chứa dữ liệu trả về
     */
    function upload($filename = '', $upload_path = '')
    {
        $config = $this->config($upload_path);
        $this->CI->load->library('upload', $config);

        if ($this->CI->upload->do_upload($filename)) {
            $data = $this->CI->upload->data();
        } else {
            $data = $this->CI->upload->display_errors();
        }
        return $data;
    }

    /**
     * Tải lên nhiều hình ảnh
     * 
     * @param string $upload_path đường dẫn đến thư mục lưu trữ
     * @param array $file mảng chứa các ảnh
     * @return array|string 
     */
    function multiple_file_upload($file, $upload_path = '')
    {
        $config = $this->config($upload_path);
        $this->CI->load->library('upload', $config);

        for ($i = 0; $i < count($file['name']); $i++) {
            $_FILES['userfile']['name']     = $file['name'][$i];
            $_FILES['userfile']['type']     = $file['type'][$i];
            $_FILES['userfile']['tmp_name'] = $file['tmp_name'][$i];
            $_FILES['userfile']['error']    = $file['error'][$i];
            $_FILES['userfile']['size']     = $file['size'][$i];
            if ($this->CI->upload->do_upload()) {
                $data = $this->CI->upload->data();
                $image_list['file_name'][] = $data['file_name'];
            } else {
                $data = $this->CI->upload->display_errors();
                return $data;
            }
        }
        return $image_list;
    }

    /**
     * cau hinh du lieu upload
     */
    function config($upload_path = '')
    {
        $config = array();
        $config['upload_path'] = $upload_path;
        $config['allowed_types'] = 'png|jpg|png';
        $config['max_size'] = '20000';

        return $config;
    }
}
